package tp.dataset;
/**
 * Clasa de start al progamului. Fiecare cerita reprezinta un apel de metoda din
 * clasa ActivityDataManager. Intre output-ul fiecarei cerinte am pus un
 * delimitator
 */
public class Main {
	public static void main(String[] args) {
		ActivityDataManager activityDataManager = new ActivityDataManager();
		// 0. Define a class MonitoredData with 3 fields: start time, end time
		// and activity as string.
		// Read the data from the file Activity.txt using streams and split each
		// line in 3 parts:
		// start_time, end_time and activity label and create a list of objects
		// of type MonitoredData.
		activityDataManager.restoreDataFromFile();

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		// 1. Count how many days of monitored data appears in the log.
		System.out.println("Number of days in log: " + activityDataManager.getNumberOfDaysInLog());

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		// 2. Determine a map of type <String, Integer> that maps to each
		// distinct action type the number of occurrences in the log. Write the
		// resulting map into a text file
		activityDataManager.getActivityAppearence();

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		// 3. Generates a data structure of type Map<Integer, Map<String,
		// Integer>> that
		// contains the activity count for each day of the log (task number 2
		// applied for each
		// day of the log)and writes the result in a text file
		activityDataManager.getActivityCountPerDay();

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		// For each line from the file map for the activity label the duration
		// recorded on that line (end_time-start_time)
		activityDataManager.labelDurationOfEachActivity();

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		// 4. Determine a data structure of the form Map<String, DateTime> that
		// maps for each activity the total duration computed over the
		// monitoring period. Filter the activities with total duration larger
		// than 10 hours. Write the result in a text file.
		activityDataManager.getFilteredTotalDurationOfActivity();

		// Filter the activities that have 90% of the monitoring records with
		// duration less than 5 minutes, collect the results in a List<String>
		// containing only the distinct activity names and write the result in a
		// text file.

		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}
}
