package tp.dataset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Clasa de tip entitate(bean). Contine infomatii despre un entry din fisierul
 * de intrare referitor la activitatile utilizatorului. Aceste informatii sunt
 * data de start, data de final si activitatea procesata de senzori
 */
public class MonitoredData {

	// Date formattere necesare pentru a afisa data in diferite formate
	public static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
	public static final DateFormat diffDateFormatter = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
	public static final DateFormat dayDateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

	private Date startDate;
	private Date endDate;
	private String activity;

	public MonitoredData(Date startDate, Date endDate, String activity) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.activity = activity;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	@Override
	public String toString() {
		return dateFormatter.format(startDate) + " - " + dateFormatter.format(endDate) + " activity: " + activity;
	}
}
