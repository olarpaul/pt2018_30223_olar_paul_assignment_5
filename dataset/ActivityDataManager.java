package tp.dataset;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Clasa ce contine implementarea metodelor de prelucrarea a informatiilor din
 * fisier folosind Java 8, lambdas si streams. Metodele din aceasta clasa se pot
 * apela direct din clasa ce contine metoda main
 */
public class ActivityDataManager {

	/**
	 * Parcurge fisierl linie cu linie si afiseaza in consola reprezentarea unei
	 * liste de obiecte MonitoredData
	 */
	public void restoreDataFromFile() {
		try {
			getListOfMonitoredDataFromFile().forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returneaza numarul total de zile dintr-un fisier de date. Se calculeaza
	 * diferenta dintre start date-ul primului element si end date-ul ultimului
	 * element din lista. Lista a fost obtinuta folosind stream si citind din
	 * fisierul de intrare
	 * 
	 * @return
	 */
	public long getNumberOfDaysInLog() {
		try {
			// Obtinem lista
			List<MonitoredData> dataList = getListOfMonitoredDataFromFile();
			// Obtinem primul si ultimul element si extragem dates
			Date firstDate = dataList.get(0).getStartDate();
			Date lastDate = dataList.get(dataList.size() - 1).getEndDate();

			// Facem diferenta
			long diffInMillies = Math.abs(lastDate.getTime() - firstDate.getTime());
			// Returnam reprezentarea in days a valorii obtinute
			return TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}

	/**
	 * Parcurge intregul fisier de intrare si numara numarul total de aparitii
	 * al fiecarei activitati in fisier
	 */
	public void getActivityAppearence() {
		try {
			// Creem un obiect filewriter pentru a scrie in fisier
			final FileWriter fw = new FileWriter("AparitiiActivity.txt");
			// Parcurgem fisierul, skip primelor 2 linii, impartim linia dupa
			// tab
			Files.lines(Paths.get("Activity.txt")).skip(2).map(line -> line.split("\\t+")[2])
					// Formam mapul de forma key = Activity, valoare = count de
					// cate ori a aparut in fisier
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
					// Scriem in fisier rezultatul
					.forEach((k, v) -> writeToFile(fw, "Item : " + k + " Count : " + v));
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adauga la sfarsitul unei linii din fisierul de intrare durata activitatii
	 * de pe randul respectiv
	 */
	public void labelDurationOfEachActivity() {
		try {
			// Parcurgem fisierul, skip primelor 2 linii, apelam pe fiecare rand
			// in parte metoda addDurationToLine si la final afisam in consola
			// noul rezultat
			Files.lines(Paths.get("Activity.txt")).skip(2).map(this::addDurationToLine).distinct()
					.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Returneaza un Map<Integer, Map<String, Long>> ce reprezinta un count pe
	 * fiecare activitate pe fiecare zi a lunii din fisier Rezultat de forma: Zi
	 * -> Map[Activitate-Count]
	 */
	public void getActivityCountPerDay() {
		try {
			// Creem un file writer pentru a scrie in fisier
			final FileWriter fw = new FileWriter("ActivitiesCountPerDay.txt");
			// Obtinem lista de monitored data din fisierul de intrare
			List<MonitoredData> list = getListOfMonitoredDataFromFile();
			// Creem map-ul rezultat, convertim lista in stream
			Map<Integer, Map<String, Long>> map = list.stream()
					// Colectam rezultatul, keya o reprezinta ziua din luna,
					// obtinuta apeland metoda getDayFromDate definita mai jos
					.collect(Collectors.groupingBy(f -> getDayFromDate(f.getStartDate()),
							// Formam al doilea map de activitate si count de
							// cate ori apare
							Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
			// Afisam map=ul in consola si in fisier
			map.forEach((k, v) -> System.out.println("Day: " + k + " Activities: " + v));
			map.forEach((k, v) -> writeToFile(fw, "Day: " + k + " Activities: " + v));
			// Inchidem fisierul
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calculeaza durata totala a unei activitati din fisierul de intrare.
	 * Filtreaza resultatele, sa returneze doar activitatile care au durata
	 * totala mai mare de 10 ore.
	 */
	public void getFilteredTotalDurationOfActivity() {
		try {
			// Creem un file writer(obiect) cu care vom scrie in fisier
			final FileWriter fw = new FileWriter("TotalDurationOfActivity.txt");
			// Obtinem lista de date din fisier
			List<MonitoredData> list = getListOfMonitoredDataFromFile();
			// Mapam fiecarei activitati, durata totala
			// Map de key = Activity si valoare = duration
			Map<String, Long> map = list.stream() // convertim lista in stream
					// ne va da cheia luand activity de pe obiectul de monitored
					// data
					.collect(Collectors.groupingBy(MonitoredData::getActivity,
							// Vaolarea reprezinta suma duration-urilor de pe
							// fiecare obiect din stream, Aplicam functia ce ne
							// da durata activitatii definita mai jos
							Collectors.summingLong(f -> getDurationOfActivity(f))));
			// Acum filtram map-ul obtinut sa contina doar elemente cu duration
			// mai mare decat 10 ore
			Map<String, Long> filteredMap = map.entrySet().stream()
					// Valoarea, adica duration sa fie mai mare decat 10 (ore)
					.filter(x -> x.getValue() > 10) 
					// Transformam stream-ul in map cu aceeasi cheie-valoare
					.collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
			// Afisam rezultate in consola
			filteredMap.forEach((k, v) -> System.out.println("Activity: " + k + " Duration: " + v + " hours"));
			// Afisam resultate in fisier
			filteredMap.forEach((k, v) -> writeToFile(fw, "Activity: " + k + " Duration: " + v + " hours"));
			// Inchidem fisierul
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Calculeaza duration-ul unei activitati sub forma de timestamp, long
	 * 
	 * @param monitoredData
	 * @return
	 */
	private Long getDurationOfActivity(MonitoredData monitoredData) {
		long diff = monitoredData.getEndDate().getTime() - monitoredData.getStartDate().getTime();
		return diff / (60 * 60 * 1000) % 24;
	}

	/**
	 * Returneaza o lista de obiected MonitoredData din fisierul de intrare
	 * folosind stream si lambda expressions
	 * 
	 * @return
	 * @throws IOException
	 */
	private List<MonitoredData> getListOfMonitoredDataFromFile() throws IOException {
		return Files.lines(Paths.get("Activity.txt")) // creem stream-ul
				.skip(2) // Primele 2 linii nu contin informatii, deci skip
				.map(line -> line.split("\\t+")) // obtinem un Stream<String[]>
													// impartind linia dupa
													// tab-uri
				.map(line -> {
					// Transformam lista de strings intr-un obiect de tip
					// MonitoredData
					try {
						return new MonitoredData(MonitoredData.dateFormatter.parse(line[0]),
								MonitoredData.dateFormatter.parse(line[1]), line[2]);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					return null;
					// Returnam o lista in final
				}).collect(Collectors.toList());
	}

	/**
	 * Returneaza ziua din luna ca si integer dintr-un obiect de tip Date,
	 * folosind Java 8
	 * 
	 * @param date
	 * @return
	 */
	private int getDayFromDate(Date date) {
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.getDayOfMonth();
	}

	/**
	 * Adauga durata in ore, minute, secunde pentru un entry de tipul: Date
	 * start Date End Activity
	 * 
	 * @param line
	 *            linia preluata din fisierul de intrare
	 * @return linia updatata cu duratia activitatii adaugata la sfarsit
	 */
	private String addDurationToLine(String line) {
		// Impartim linia
		String[] splittedLine = line.split("\\t+");
		try {
			// Luam data de start si end folosind formatterul
			Date endTime = MonitoredData.dateFormatter.parse(splittedLine[1]);
			Date startTime = MonitoredData.dateFormatter.parse(splittedLine[0]);
			// Calculam diferenta
			long diff = endTime.getTime() - startTime.getTime();
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000) % 24;

			// Adaugam la sfarsitul stringului in forma corespunzatoare
			if (diffHours != 0) {
				line += "\t(" + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds)";
			} else if (diffHours == 0 && diffMinutes == 0) {
				line += "\t(" + diffSeconds + " seconds)";
			} else {
				line += "\t(" + diffMinutes + " minutes, " + diffSeconds + " seconds)";
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return line;
	}

	/**
	 * Metoda care scrie in fisier cate o linie de continut
	 * 
	 * @param fw
	 *            FileWriter in care se va scrie, obiectul este deja creat
	 *            intr-un fisier, path-ul specificat unde se apeleaza
	 * @param content
	 *            Continutul unui rand de scris in fisier
	 */
	private void writeToFile(FileWriter fw, String content) {
		try {
			// Scriem continutul pe cate un rand
			fw.write(String.format("%s%n", content));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
